#include "HWWAnalysisCode/ZHCand.h"

#include <limits>
#include <string.h>
#include <iostream>
#include <map>
#include "TMath.h"

#include "xAODParticleEvent/CompositeParticleContainer.h"
#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODMissingET/MissingETContainer.h"


// uncomment the following line to enable debug printouts
#define _DEBUG_
// you can perform debug printouts with statements like this
// DEBUG("error number %d occurred",someInteger);

// be careful to not move the _DEBUG_ flag behind the following line
// otherwise, it will show no effect
#include "QFramework/TQLibrary.h"

ClassImp(ZHCand)

//______________________________________________________________________________________________

ZHCand::ZHCand(){
  // default constructor
  DEBUGclass("default constructor called");
}

//______________________________________________________________________________________________

ZHCand::~ZHCand(){
  // default destructor
  DEBUGclass("destructor called");
} 


#define XAOD_STANDALONE 1
// put here any EDM includes you might need, e.g.
/* example block
#include "xAODParticleEvent/CompositeParticleContainer.h"
*/

//______________________________________________________________________________________________

bool ZHCand::makeCache() const {
  // function that chaches vector that is later retrieved with function getVector()
  // called on every event for every cut and histogram
  
  DEBUGclass("=============================== entering function ===========================");

  // the TQEventObservable only works in an ASG RELEASE, hence
  // we encapsulate the implementation in an ifdef/ifndef block
  #ifndef HAS_XAOD
  #warning "using plain ROOT compilation scheme - please add an ASG Analysis Release in order to use this feature!"
  return std::numeric_limits<double>::quiet_NaN();
  #else
  // in the rest of this function, you should retrieve the data and calculate your return value
  // here is the place where most of your custom code should go
  // a couple of comments should guide you through the process
  // when writing your code, please keep in mind that this code can be executed several times on every event
  // make your code efficient. catch all possible problems. when in doubt, contact experts!

  if (this->getCurrentEntry() == this->fCachedEntry) {
    return true; //nothing to do here, return value already cached
  }

  // clear the old cache before recalculating it for new event
  /* example block
     myvector.clear();
  */

     myvector.clear();
  // first, you can retrieve the data members you need with the 'retrieve' method:
  /* example block:
  // Retrieve CompositeParticle container
  const xAOD::CompositeParticleContainer *cand = 0;
  if(!this->fEvent->retrieve(cand, this->mContName.Data()).isSuccess()){
  DEBUGclass("failed to retrieve candidates!");
  return false;
  }
   */
  // Retrieve CompositeParticle container
  const xAOD::CompositeParticleContainer *cand = 0;
  if(!this->fEvent->retrieve(cand, this->mContName.Data()).isSuccess()){
  DEBUGclass("failed to retrieve candidates!");
  return false;
  }
  DEBUGclass(" sucess retrieved candidates! %s",this->mContName.Data());
  DEBUGclass(" cand->size(): %i",cand->size());
  
  // after you have retrieved your data members, you can proceed to calculate the return value
  // probably, you first have to retrieve an element from the container
  /* example block:
  const xAOD::CompositeParticle *Evt = cand->at(0);
  */
  int nmuon=0;
  int nmuon_p=0;
  int nmuon_m=0;
  int nele=0;
  int nele_p=0;
  int nele_m=0;
  int njet=0;
  int nlep=0; 
  // part/otherpart, index, pt
  std::vector<std::tuple<std::string, int, float>>vecjet;
  std::vector<std::tuple<std::string, int, float>>vecmuon;
  std::vector<std::tuple<std::string, int, float>>vecmuon_p;
  std::vector<std::tuple<std::string, int, float>>vecmuon_m;
  std::vector<std::tuple<std::string, int, float>>vecele;
  std::vector<std::tuple<std::string, int, float>>vecele_p;
  std::vector<std::tuple<std::string, int, float>>vecele_m;
  // ele/muon, part/otherpart,index, pt
  std::vector<std::tuple<std::string, std::string, int, float>>veclep;
    
//1st loop
  DEBUGclass("============================ event info start ===============================");
  const xAOD::CompositeParticle *Evt = cand->at(0);
  for (std::size_t n = 0; n < Evt->nParts(); ++n){
    if (Evt->part(n)->type()==xAOD::Type::Jet){
      njet++;
      const xAOD::Jet *jet = Evt->jet(n);
      vecjet.push_back({"part", n, jet->pt() });
    }
    if (Evt->part(n)->type()==xAOD::Type::Muon){
      nmuon++;
      nlep++;
      const xAOD::Muon *muon = Evt->muon(n);
      int muon_charge = muon->charge();
      if (muon_charge==1){vecmuon_p.push_back({"part", n, muon->pt() });nmuon_p++;}
      if (muon_charge==-1){vecmuon_m.push_back({"part", n, muon->pt() });nmuon_m++;}
      vecmuon.push_back({"part", n, muon->pt() });
      veclep.push_back({"muon", "part", n, muon->pt() });
      //DEBUGclass("part :%i  muon eta:%f ,muon phi:%f , pt:%f ",n, muon->eta(),muon->phi(),muon->pt());
    }
    if (Evt->part(n)->type()==xAOD::Type::Electron){
      nele++;
      nlep++;
      const xAOD::Electron* ele=Evt->electron(n);
      int ele_charge = ele->charge();
      if (ele_charge==1){vecele_p.push_back({"part", n, ele->pt() });nele_p++;}
      if (ele_charge==-1){vecele_m.push_back({"part", n, ele->pt() });nele_m++;}
      vecele.push_back({"part", n, ele->pt() });
      veclep.push_back({"ele", "part", n, ele->pt() });
    }    
  }
  for (std::size_t n = 0; n < Evt->nOtherParts(); ++n){
    // if (Evt->otherPart(n)->type()==xAOD::Type::Jet){
    //   njet++;
    //   const xAOD::Jet *jet = Evt->otherJet(n);
    //   vecjet.push_back({"otherpart", n, jet->pt() });
    //   if (jet->pt()>200000){
    //     njet_pt200++;
    //     vecjet_pt200.push_back({"otherpart", n, jet->pt() });
    //   }
    // }
    if (Evt->otherPart(n)->type()==xAOD::Type::Muon){
      nmuon++;
      nlep++;
      const xAOD::Muon *muon = Evt->otherMuon(n);
      int muon_charge = muon->charge();
      if (muon_charge==1){vecmuon_p.push_back({"otherpart", n, muon->pt() });nmuon_p++;}
      if (muon_charge==-1){vecmuon_m.push_back({"otherpart", n, muon->pt() });nmuon_m++;}
      vecmuon.push_back({"otherpart", n, muon->pt() });
      veclep.push_back({"muon", "otherpart", n, muon->pt() });
    }
    if (Evt->otherPart(n)->type()==xAOD::Type::Electron){
      nele++;
      nlep++;
      const xAOD::Electron* ele=Evt->otherElectron(n);
      int ele_charge = ele->charge();
      if (ele_charge==1){vecele_p.push_back({"otherpart", n, ele->pt() });nele_p++;}
      if (ele_charge==-1){vecele_m.push_back({"otherpart", n, ele->pt() });nele_m++;}
      vecele.push_back({"otherpart", n, ele->pt() });
      veclep.push_back({"ele", "otherpart", n, ele->pt() });
    }    
  }

  const xAOD::MissingET *METCand = Evt->missingET();
  
  DEBUGclass("retrieve done");
  
  if(njet>=1) std::sort(vecjet.begin(), vecjet.end(), cmp1 );
  
  if(nmuon>=1) std::sort(vecmuon.begin(), vecmuon.end(), cmp1 );
  if(nmuon_p>=1) std::sort(vecmuon_p.begin(), vecmuon_p.end(), cmp1 );
  if(nmuon_m>=1) std::sort(vecmuon_m.begin(), vecmuon_m.end(), cmp1 );
  
  if(nele>=1) std::sort(vecele.begin(), vecele.end(), cmp1 );
  if(nele_p>=1) std::sort(vecele_p.begin(), vecele_p.end(), cmp1 );
  if(nele_m>=1) std::sort(vecele_m.begin(), vecele_m.end(), cmp1 );
  
  if(nlep>=1) std::sort(veclep.begin(), veclep.end(), cmp2 );
  DEBUGclass("sort done");

  //jet 
  float jet1_pt=9999999;
  float jet1_eta=9999999;
  float jet1_phi=9999999;
  float jet1_mass=9999999;
  const xAOD::Jet* jet1;
  TLorentzVector jet1_TLV;
  if(njet>=1) {
    auto type = std::get<0>(vecjet.at(0));
    int index = std::get<1>(vecjet.at(0));
    if(type == "part") jet1 = Evt->jet(index);
    jet1_TLV = jet1->p4();
    jet1_pt = jet1->pt();
    jet1_eta = jet1->eta();
    jet1_phi = jet1->phi();
    jet1_mass = jet1_TLV.M();
    DEBUGclass("jet1_pt:%f",jet1_pt);
    DEBUGclass("jet1_eta:%f",jet1_eta);
    DEBUGclass("jet1_phi:%f",jet1_phi);
    DEBUGclass("jet1_mass:%f",jet1_mass);
  }
  
  float jet2_pt=9999999;
  float jet2_eta=9999999;
  float jet2_phi=9999999;
  float jet2_mass=9999999;
  const xAOD::Jet* jet2;
  TLorentzVector jet2_TLV;
  if(njet>=2) {
    auto type = std::get<0>(vecjet.at(1));
    int index = std::get<1>(vecjet.at(1));
    if(type == "part") jet2 = Evt->jet(index);
    jet2_TLV = jet2->p4();
    jet2_pt = jet2->pt();
    jet2_eta = jet2->eta();
    jet2_phi = jet2->phi();
    jet2_mass = jet2_TLV.M();
    DEBUGclass("jet2_pt:%f",jet2_pt);
    DEBUGclass("jet2_eta:%f",jet2_eta);
    DEBUGclass("jet2_phi:%f",jet2_phi);
    DEBUGclass("jet2_mass:%f",jet2_mass);
  }
  DEBUGclass("jet done");
  
  //ele 
  float ele1_pt=9999999;
  float ele1_eta=9999999;
  float ele1_phi=9999999;
  float ele1_mass=9999999;
  int ele1_charge=9999999;
  const xAOD::Electron* ele1;
  TLorentzVector ele1_TLV;
  if(nele>=1){
    int index = std::get<1>(vecele.at(0));
    auto type = std::get<0>(vecele.at(0));
    if(type == "part") ele1 = Evt->electron(index);
    if(type == "otherpart") ele1 = Evt->otherElectron(index);
    ele1_pt = ele1->pt();
    ele1_eta =  ele1->eta();
    ele1_phi =  ele1->phi();
    ele1_charge =  ele1->charge();
    ele1_TLV =  ele1->p4();
    ele1_mass = ele1_TLV.M();
  } 

  float ele2_pt=9999999;
  float ele2_eta=9999999;
  float ele2_phi=9999999;
  float ele2_mass=9999999;
  int ele2_charge=9999999;
  const xAOD::Electron* ele2;
  TLorentzVector ele2_TLV;
  if(nele>=2){
    int index = std::get<1>(vecele.at(1));
    auto type = std::get<0>(vecele.at(1));
    if(type == "part") ele2 = Evt->electron(index);
    if(type == "otherpart") ele2 = Evt->otherElectron(index);
    ele2_pt = ele2->pt();
    ele2_eta =  ele2->eta();
    ele2_phi =  ele2->phi();
    ele2_charge =  ele2->charge();
    ele2_TLV =  ele2->p4();
    ele2_mass = ele2_TLV.M();
  } 

  const xAOD::Electron* ele1_p;
  TLorentzVector ele1_p_TLV;
  if(nele_p>=1){
    int index = std::get<1>(vecele_p.at(0));
    auto type = std::get<0>(vecele_p.at(0));
    if(type == "part") ele1_p = Evt->electron(index);
    if(type == "otherpart") ele1_p = Evt->otherElectron(index);
    ele1_p_TLV =  ele1_p->p4();
  } 

  const xAOD::Electron* ele1_m;
  TLorentzVector ele1_m_TLV;
  if(nele_m>=1){
    int index = std::get<1>(vecele_m.at(0));
    auto type = std::get<0>(vecele_m.at(0));
    if(type == "part") ele1_m = Evt->electron(index);
    if(type == "otherpart") ele1_m = Evt->otherElectron(index);
    ele1_m_TLV =  ele1_m->p4();
  }
  DEBUGclass("ele done");
  
  //muon
  float muon1_pt=9999999;
  float muon1_eta=9999999;
  float muon1_phi=9999999;
  float muon1_mass=9999999;
  int muon1_charge=9999999;
  const xAOD::Muon* muon1;
  TLorentzVector muon1_TLV;
  if(nmuon>=1){
    int index = std::get<1>(vecmuon.at(0));
    auto type = std::get<0>(vecmuon.at(0));
    if(type == "part") muon1 = Evt->muon(index);
    if(type == "otherpart") muon1 = Evt->otherMuon(index);
    muon1_pt = muon1->pt();
    muon1_eta =  muon1->eta();
    muon1_phi =  muon1->phi();
    muon1_charge =  muon1->charge();
    muon1_TLV =  muon1->p4();
    muon1_mass = muon1_TLV.M();
  }
  
  float muon2_pt=9999999;
  float muon2_eta=9999999;
  float muon2_phi=9999999;
  float muon2_mass=9999999;
  float muon2_charge=9999999;
  const xAOD::Muon* muon2;
  TLorentzVector muon2_TLV;
  if(nmuon>=2){
    int index = std::get<1>(vecmuon.at(1));
    auto type = std::get<0>(vecmuon.at(1));
    if(type == "part") muon2 = Evt->muon(index);
    if(type == "otherpart") muon2 = Evt->otherMuon(index);
    muon2_pt = muon2->pt();
    muon2_eta =  muon2->eta();
    muon2_phi =  muon2->phi();
    muon2_charge =  muon2->charge();
    muon2_TLV =  muon2->p4();
    muon2_mass = muon2_TLV.M();
  }

  float muon3_pt=9999999;
  float muon3_eta=9999999;
  float muon3_phi=9999999;
  float muon3_mass=9999999;
  int muon3_charge=9999999;
  const xAOD::Muon* muon3;
  TLorentzVector muon3_TLV;
  if(nmuon>=3){
    int index = std::get<1>(vecmuon.at(2));
    auto type = std::get<0>(vecmuon.at(2));
    if(type == "part") muon3 = Evt->muon(index);
    if(type == "otherpart") muon3 = Evt->otherMuon(index);
    muon3_pt = muon3->pt();
    muon3_eta =  muon3->eta();
    muon3_phi =  muon3->phi();
    muon3_charge =  muon3->charge();
    muon3_TLV =  muon3->p4();
    muon3_mass = muon3_TLV.M();
  }

  const xAOD::Muon* muon1_p;
  TLorentzVector muon1_p_TLV;
  if(nmuon_p>=1){
    int index = std::get<1>(vecmuon_p.at(0));
    auto type = std::get<0>(vecmuon_p.at(0));
    if(type == "part") muon1_p = Evt->muon(index);
    if(type == "otherpart") muon1_p = Evt->otherMuon(index);
    muon1_p_TLV =  muon1_p->p4();
  } 

  const xAOD::Muon* muon1_m;
  TLorentzVector muon1_m_TLV;
  if(nmuon_m>=1){
    int index = std::get<1>(vecmuon_m.at(0));
    auto type = std::get<0>(vecmuon_m.at(0));
    if(type == "part") muon1_m = Evt->muon(index);
    if(type == "otherpart") muon1_m = Evt->otherMuon(index);
    muon1_m_TLV =  muon1_m->p4();
  }
  DEBUGclass("muon done");

  //uPair
  std::vector<std::tuple<float,std::string,std::string,int,int>> vec_uPair;
  for (std::size_t i = 0; i < nmuon_p; ++i) {
    const xAOD::Muon* muon_p;
    auto up_type = std::get<0>(vecmuon_p.at(i));
    int up_index = std::get<1>(vecmuon_p.at(i));
    if(up_type == "part") muon_p = Evt->muon(up_index);
    if(up_type == "otherpart") muon_p = Evt->otherMuon(up_index);
    auto muon_p_TLV = muon_p->p4();
    for (std::size_t i = 0; i < nmuon_m; ++i) {
      const xAOD::Muon* muon_m;
      auto um_type = std::get<0>(vecmuon_m.at(i));
      int um_index = std::get<1>(vecmuon_m.at(i));
      if(um_type == "part") muon_m = Evt->muon(um_index);
      if(um_type == "otherpart") muon_m = Evt->otherMuon(um_index);
      auto muon_m_TLV = muon_m->p4();

      auto uPair_TLV = muon_p_TLV+muon_m_TLV;
      vec_uPair.push_back({uPair_TLV.M(),up_type,um_type,up_index,um_index});
      DEBUGclass("M_uPair:%f",uPair_TLV.M());
    }
  }
  DEBUGclass("uPair done");

  //ePair
  std::vector<std::tuple<float,std::string,std::string,int,int>> vec_ePair;
  for (std::size_t i = 0; i < nele_p; ++i) {
    const xAOD::Electron* ele_p;
    auto ep_type = std::get<0>(vecele_p.at(i));
    int ep_index = std::get<1>(vecele_p.at(i));
    if(ep_type == "part") ele_p = Evt->electron(ep_index);
    if(ep_type == "otherpart") ele_p = Evt->otherElectron(ep_index);
    auto ele_p_TLV = ele_p->p4();
    for (std::size_t i = 0; i < nele_m; ++i) {
      const xAOD::Electron* ele_m;
      auto em_type = std::get<0>(vecele_m.at(i));
      int em_index = std::get<1>(vecele_m.at(i));
      if(em_type == "part") ele_m = Evt->electron(em_index);
      if(em_type == "otherpart") ele_m = Evt->otherElectron(em_index);
      auto ele_m_TLV = ele_m->p4();

      auto ePair_TLV = ele_p_TLV+ele_m_TLV;
      vec_ePair.push_back({ePair_TLV.M(),ep_type,em_type,ep_index,em_index});
      DEBUGclass("M_ePair:%f",ePair_TLV.M());
    }
  }
  DEBUGclass("ePair done");

  //M_Zc_uPair
  float Zmass = 91187.6;//MeV
  float Hmass = 125250;//MeV
  float min_mass_diff_Z_uPair = 9999999;
  std::tuple<float,std::string,std::string,int,int> Zc_uPair;
  float M_Zc_uPair = 9999999;
  if (vec_uPair.size()>=1){
    for (std::size_t i = 0; i < vec_uPair.size(); ++i) {
      float M_uPair = std::get<0>(vec_uPair.at(i));
      float mass_diff = std::abs(M_uPair-Zmass);
      if(mass_diff<min_mass_diff_Z_uPair) {
        min_mass_diff_Z_uPair=mass_diff; 
        Zc_uPair = vec_uPair.at(i);
      }
    }
    M_Zc_uPair = std::get<0>(Zc_uPair);
    DEBUGclass("M_Zc_uPair:%f",M_Zc_uPair);
  }
  DEBUGclass("M_Zc_uPair done");

  //M_Zc_ePair
  float min_mass_diff_Z_ePair = 9999999;
  std::tuple<float,std::string,std::string,int,int> Zc_ePair;
  float M_Zc_ePair = 9999999;
  if (vec_ePair.size()>=1){
    for (std::size_t i = 0; i < vec_ePair.size(); ++i) {
      float M_ePair = std::get<0>(vec_ePair.at(i));
      float mass_diff = std::abs(M_ePair-Zmass);
      if(mass_diff<min_mass_diff_Z_ePair) {
        min_mass_diff_Z_ePair=mass_diff; 
        Zc_ePair = vec_ePair.at(i);
      }
    }
    M_Zc_ePair = std::get<0>(Zc_ePair);
    DEBUGclass("M_Zc_ePair:%f",M_Zc_ePair);
  }
  DEBUGclass("M_Zc_ePair done");

  //lep_no_Zc_uPair
  auto veclep_no_Zc_uPair = veclep;
  if (vec_uPair.size()>=1){
    auto uPair_p_type = std::get<1>(Zc_uPair);
    auto uPair_m_type = std::get<2>(Zc_uPair);
    int uPair_p_index = std::get<3>(Zc_uPair);
    int uPair_m_index = std::get<4>(Zc_uPair);
  
    int uPair_p_posi=0;
    int uPair_m_posi=0;
    if(nmuon_p>=1 && nmuon_m>=1 && nlep>=3){
      for (std::size_t i = 0; i < nlep; ++i) {
        auto flavor = std::get<0>(veclep.at(i));
        auto type = std::get<1>(veclep.at(i));
        int index = std::get<2>(veclep.at(i));
        if(flavor=="muon" && type==uPair_p_type && index== uPair_p_index){uPair_p_posi=i;}
        if(flavor=="muon" && type==uPair_m_type && index== uPair_m_index){uPair_m_posi=i;}
      }
      if (uPair_p_posi>uPair_m_posi){
        veclep_no_Zc_uPair.erase(veclep_no_Zc_uPair.begin()+uPair_p_posi);
        veclep_no_Zc_uPair.erase(veclep_no_Zc_uPair.begin()+uPair_m_posi);
      }
      if (uPair_p_posi<uPair_m_posi){
        veclep_no_Zc_uPair.erase(veclep_no_Zc_uPair.begin()+uPair_m_posi);
        veclep_no_Zc_uPair.erase(veclep_no_Zc_uPair.begin()+uPair_p_posi);
      }
    }
    DEBUGclass("n_lep:%f",veclep.size());
    DEBUGclass("n_lep_no_Zc_uPair:%f",veclep_no_Zc_uPair.size());
  }
  DEBUGclass("lep_no_Zc_uPair done");

  //lep_no_Zc_ePair
  auto veclep_no_Zc_ePair = veclep;
  if (vec_ePair.size()>=1){
    auto ePair_p_type = std::get<1>(Zc_ePair);
    auto ePair_m_type = std::get<2>(Zc_ePair);
    int ePair_p_index = std::get<3>(Zc_ePair);
    int ePair_m_index = std::get<4>(Zc_ePair); 
    int ePair_p_posi=0;
    int ePair_m_posi=0;
    if(nele_p>=1 && nele_m>=1 && nlep>=3){
      for (std::size_t i = 0; i < nlep; ++i) {
        auto flavor = std::get<0>(veclep.at(i));
        auto type = std::get<1>(veclep.at(i));
        int index = std::get<2>(veclep.at(i));
        if(flavor=="ele" && type==ePair_p_type && index== ePair_p_index){ePair_p_posi=i;}
        if(flavor=="ele" && type==ePair_m_type && index== ePair_m_index){ePair_m_posi=i;}
      }
      if (ePair_p_posi>ePair_m_posi){
        veclep_no_Zc_ePair.erase(veclep_no_Zc_ePair.begin()+ePair_p_posi);
        veclep_no_Zc_ePair.erase(veclep_no_Zc_ePair.begin()+ePair_m_posi);
      }
      if (ePair_p_posi<ePair_m_posi){
        veclep_no_Zc_ePair.erase(veclep_no_Zc_ePair.begin()+ePair_m_posi);
        veclep_no_Zc_ePair.erase(veclep_no_Zc_ePair.begin()+ePair_p_posi);
      }
    }
  }
  DEBUGclass("lep_no_Zc_ePair done");

  //noZcUpair_Hlep
  TLorentzVector noZcUpair_Hlep_TLV;
  if (veclep_no_Zc_uPair.size()>=1){
    auto flavor = std::get<0>(veclep_no_Zc_uPair.at(0));
    auto type = std::get<1>(veclep_no_Zc_uPair.at(0));
    int index = std::get<2>(veclep_no_Zc_uPair.at(0));
    
    const xAOD::Muon* muon;
    if(flavor=="muon"){
      if(type == "part") muon = Evt->muon(index);
      if(type == "otherpart") muon = Evt->otherMuon(index);
      noZcUpair_Hlep_TLV = muon->p4();
    }

    const xAOD::Electron* ele;
    if(flavor=="ele"){
      if(type == "part") ele = Evt->electron(index);
      if(type == "otherpart") ele = Evt->otherElectron(index);
      noZcUpair_Hlep_TLV = ele->p4();
    }
  }

  //noZcEpair_Hlep
  TLorentzVector noZcEpair_Hlep_TLV;
  if (veclep_no_Zc_ePair.size()>=1){
    auto flavor = std::get<0>(veclep_no_Zc_ePair.at(0));
    auto type = std::get<1>(veclep_no_Zc_ePair.at(0));
    int index = std::get<2>(veclep_no_Zc_ePair.at(0));

    const xAOD::Muon* muon;
    if(flavor=="muon"){
      if(type == "part") muon = Evt->muon(index);
      if(type == "otherpart") muon = Evt->otherMuon(index);
      noZcEpair_Hlep_TLV = muon->p4();
    }

    const xAOD::Electron* ele;
    if(flavor=="ele"){
      if(type == "part") ele = Evt->electron(index);
      if(type == "otherpart") ele = Evt->otherElectron(index);
      noZcEpair_Hlep_TLV = ele->p4();
    }
  }
  
  //M_jet1_noZcUpair_Hlep
  float M_jet1_noZcUpair_Hlep=9999999;
  float dR_jet1_noZcUpair_Hlep=9999999;
  if (veclep_no_Zc_uPair.size()>=1){
    TLorentzVector jet1_noZcUpair_Hlep_TLV = jet1_TLV + noZcUpair_Hlep_TLV; 
    M_jet1_noZcUpair_Hlep = jet1_noZcUpair_Hlep_TLV.M();
    dR_jet1_noZcUpair_Hlep = jet1_TLV.DeltaR(noZcUpair_Hlep_TLV);
  }

  //M_jet1_noZcEpair_Hlep
  float M_jet1_noZcEpair_Hlep=9999999;
  float dR_jet1_noZcEpair_Hlep=9999999;
  if (veclep_no_Zc_ePair.size()>=1){
    TLorentzVector jet1_noZcEpair_Hlep_TLV = jet1_TLV + noZcEpair_Hlep_TLV; 
    M_jet1_noZcEpair_Hlep = jet1_noZcEpair_Hlep_TLV.M();
    dR_jet1_noZcEpair_Hlep = jet1_TLV.DeltaR(noZcEpair_Hlep_TLV);
  }
  
  //lep
  float lep1_pt=9999999;
  float lep1_eta=9999999;
  float lep1_phi=9999999;
  int lep1_charge=9999999;
  int lep1_flavor=9999999;
  TLorentzVector lep1_TLV;
  if(nlep>=1) {
    lep1_pt = std::get<3>(veclep.at(0));
    auto flavor = std::get<0>(veclep.at(0));
    auto type = std::get<1>(veclep.at(0));
    int index = std::get<2>(veclep.at(0));
    const xAOD::Muon* muon;
    if (flavor=="muon") {
      lep1_flavor = 0;
      if(type == "part") muon = Evt->muon(index);
      if(type == "otherpart") muon = Evt->otherMuon(index);
      auto lep1=muon;
      lep1_eta = lep1->eta();
      lep1_phi = lep1->phi();
      lep1_charge = lep1->charge();
      lep1_TLV = lep1->p4();
    }
    const xAOD::Electron* ele;
    if (flavor=="ele") {
      lep1_flavor = 1;
      if(type == "part") ele = Evt->electron(index);
      if(type == "otherpart") ele = Evt->otherElectron(index);
      auto lep1=ele;
      lep1_eta = lep1->eta();
      lep1_phi = lep1->phi();
      lep1_charge = lep1->charge();
      lep1_TLV = lep1->p4();
    }
  }

  float lep2_pt=9999999;
  float lep2_eta=9999999;
  float lep2_phi=9999999;
  int lep2_charge=9999999;
  int lep2_flavor=9999998;
  TLorentzVector lep2_TLV;
  if(nlep>=2) {
    lep2_pt = std::get<3>(veclep.at(1));
    auto flavor = std::get<0>(veclep.at(1));
    auto type = std::get<1>(veclep.at(1));
    int index = std::get<2>(veclep.at(1));
    const xAOD::Muon* muon;
    if (flavor=="muon") {
      lep2_flavor = 0;
      if(type == "part") muon = Evt->muon(index);
      if(type == "otherpart") muon = Evt->otherMuon(index);
      auto lep2=muon;
      lep2_eta = lep2->eta();
      lep2_phi = lep2->phi();
      lep2_charge = lep2->charge();
      lep2_TLV = lep2->p4();
    }
    const xAOD::Electron* ele;
    if (flavor=="ele") {
      lep2_flavor = 1;
      if(type == "part") ele = Evt->electron(index);
      if(type == "otherpart") ele = Evt->otherElectron(index);
      auto lep2=ele;
      lep2_eta = lep2->eta();
      lep2_phi = lep2->phi();
      lep2_charge = lep2->charge();
      lep2_TLV = lep2->p4();
    }
  }

  float lep3_pt=9999999;
  float lep3_eta=9999999;
  float lep3_phi=9999999;
  int lep3_charge=9999999;
  int lep3_flavor=9999997;
  TLorentzVector lep3_TLV;
  if(nlep>=3) {
    lep3_pt = std::get<3>(veclep.at(2));
    auto flavor = std::get<0>(veclep.at(2));
    auto type = std::get<1>(veclep.at(2));
    int index = std::get<2>(veclep.at(2));
    const xAOD::Muon* muon;
    if (flavor=="muon") {
      lep3_flavor = 0;
      if(type == "part") muon = Evt->muon(index);
      if(type == "otherpart") muon = Evt->otherMuon(index);
      auto lep3=muon;
      lep3_eta = lep3->eta();
      lep3_phi = lep3->phi();
      lep3_charge = lep3->charge();
      lep3_TLV = lep3->p4();
    }
    const xAOD::Electron* ele;
    if (flavor=="ele") {
      lep3_flavor = 1;
      if(type == "part") ele = Evt->electron(index);
      if(type == "otherpart") ele = Evt->otherElectron(index);
      auto lep3=ele;
      lep3_eta = lep3->eta();
      lep3_phi = lep3->phi();
      lep3_charge = lep3->charge();
      lep3_TLV = lep3->p4();
    }
  }
  DEBUGclass("lep done");

  DEBUGclass("out jet1_TLV.Pt():%f",jet1_TLV.Pt());
  DEBUGclass("out jet1_TLV.M():%f",jet1_TLV.M());
  DEBUGclass("out jet1_TLV.E():%f",jet1_TLV.E());
  //jet1 dR,M,mT
  float M_jet1_muon1 = 9999999;
  float mT_jet1_muon1 = 9999999;
  float dR_jet1_muon1 = 9999999;
  float M_jet1_muon2 = 9999999;
  float mT_jet1_muon2 = 9999999;
  float dR_jet1_muon2 = 9999999;
  float M_jet1_muon3 = 9999999;
  float mT_jet1_muon3 = 9999999;
  float dR_jet1_muon3 = 9999999;
  float M_jet1_ele1 = 9999999;
  float mT_jet1_ele1 = 9999999;
  float dR_jet1_ele1 = 9999999;
  float M_jet1_ele2 = 9999999;
  float mT_jet1_ele2 = 9999999;
  float dR_jet1_ele2 = 9999999;

  float mT_jet1_noZcUpair_Hlep = 9999999;
  float mT_jet1_noZcEpair_Hlep = 9999999;
  
  if(njet>=1 && veclep_no_Zc_uPair.size()>=1){
    auto H_TLV = jet1_TLV+noZcUpair_Hlep_TLV;
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet1_noZcUpair_Hlep  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));
    DEBUGclass("mT_jet1_noZcUpair_Hlep:%f",mT_jet1_noZcUpair_Hlep);
  }

  if(njet>=1 && veclep_no_Zc_ePair.size()>=1){
    auto H_TLV = jet1_TLV+noZcEpair_Hlep_TLV;
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet1_noZcEpair_Hlep  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));
    DEBUGclass("mT_jet1_noZcEpair_Hlep:%f",mT_jet1_noZcEpair_Hlep);
  }
  
  if(njet>=1 && nmuon>=1){
    auto H_TLV = jet1_TLV+muon1_TLV;
    M_jet1_muon1 = H_TLV.M();
    DEBUGclass("M_jet1_muon1:%f",M_jet1_muon1);
    
    DEBUGclass("jet1_TLV.Pt():%f",jet1_TLV.Pt());
    DEBUGclass("jet1_TLV.M():%f",jet1_TLV.M());
    DEBUGclass("jet1_TLV.E():%f",jet1_TLV.E());
    
    DEBUGclass("muon1_TLV.Pt():%f",muon1_TLV.Pt());
    DEBUGclass("muon1_TLV.M():%f",muon1_TLV.M());
    DEBUGclass("muon1_TLV.E():%f",muon1_TLV.E());
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet1_muon1  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));
    DEBUGclass("H_TLV.Pt():%f",H_TLV.Pt());
    DEBUGclass("ET:%f",ET);
    DEBUGclass("METCand->met():%f",METCand->met());
    DEBUGclass("mT_jet1_muon1:%f",mT_jet1_muon1);
    double dEta_jet1_muon1 = fabs(muon1_eta-jet1_eta);
    double dPhi_jet1_muon1 = fabs(muon1_phi-jet1_phi);
    if (dPhi_jet1_muon1 >= TMath::Pi()) dPhi_jet1_muon1 = 2*TMath::Pi()-dPhi_jet1_muon1;
    dR_jet1_muon1 = sqrt(dEta_jet1_muon1*dEta_jet1_muon1+dPhi_jet1_muon1*dPhi_jet1_muon1);
  }

  
  if(njet>=1 && nmuon>=2){
    auto H_TLV = jet1_TLV+muon2_TLV;
    M_jet1_muon2 = H_TLV.M();
    DEBUGclass("M_jet1_muon2:%f",M_jet1_muon2);
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet1_muon2  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));
    DEBUGclass("mT_jet1_muon2:%f",mT_jet1_muon2);
    double dEta_jet1_muon2 = fabs(muon2_eta-jet1_eta);
    double dPhi_jet1_muon2 = fabs(muon2_phi-jet1_phi);
    if (dPhi_jet1_muon2 >= TMath::Pi()) dPhi_jet1_muon2 = 2*TMath::Pi()-dPhi_jet1_muon2;
    dR_jet1_muon2 = sqrt(dEta_jet1_muon2*dEta_jet1_muon2+dPhi_jet1_muon2*dPhi_jet1_muon2);
  }

  
  if(njet>=1 && nmuon>=3){
    auto H_TLV = jet1_TLV+muon3_TLV;
    M_jet1_muon3 = H_TLV.M();
    DEBUGclass("M_jet1_muon3:%f",M_jet1_muon3);
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet1_muon3  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));

    double dEta_jet1_muon3 = fabs(muon3_eta-jet1_eta);
    double dPhi_jet1_muon3 = fabs(muon3_phi-jet1_phi);
    if (dPhi_jet1_muon3 >= TMath::Pi()) dPhi_jet1_muon3 = 2*TMath::Pi()-dPhi_jet1_muon3;
    dR_jet1_muon3 = sqrt(dEta_jet1_muon3*dEta_jet1_muon3+dPhi_jet1_muon3*dPhi_jet1_muon3);
  }

  
  if(njet>=1 && nele>=1){
    auto H_TLV = jet1_TLV+ele1_TLV;
    M_jet1_ele1 = H_TLV.M();
    DEBUGclass("M_jet1_ele1:%f",M_jet1_ele1);
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet1_ele1  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));
    DEBUGclass("ele1_TLV.Pt():%f",ele1_TLV.Pt());
    DEBUGclass("ele1_TLV.M():%f",ele1_TLV.M());
    DEBUGclass("ele1_TLV.E():%f",ele1_TLV.E());

    double dEta_jet1_ele1 = fabs(ele1_eta-jet1_eta);
    double dPhi_jet1_ele1 = fabs(ele1_phi-jet1_phi);
    if (dPhi_jet1_ele1 >= TMath::Pi()) dPhi_jet1_ele1 = 2*TMath::Pi()-dPhi_jet1_ele1;
    dR_jet1_ele1 = sqrt(dEta_jet1_ele1*dEta_jet1_ele1+dPhi_jet1_ele1*dPhi_jet1_ele1);
  }

  if(njet>=1 && nele>=2){
    auto H_TLV = jet1_TLV+ele2_TLV;
    M_jet1_ele2 = H_TLV.M();
    DEBUGclass("M_jet1_ele2:%f",M_jet1_ele2);
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet1_ele2  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));

    double dEta_jet1_ele2 = fabs(ele2_eta-jet1_eta);
    double dPhi_jet1_ele2 = fabs(ele2_phi-jet1_phi);
    if (dPhi_jet1_ele2 >= TMath::Pi()) dPhi_jet1_ele2 = 2*TMath::Pi()-dPhi_jet1_ele2;
    dR_jet1_ele2 = sqrt(dEta_jet1_ele2*dEta_jet1_ele2+dPhi_jet1_ele2*dPhi_jet1_ele2);
  }
  DEBUGclass("jet1 dR,M,mT done");

  //jet2 dR,M,mT
  float M_jet2_muon1 = 9999999;
  float mT_jet2_muon1 = 9999999;
  float dR_jet2_muon1 = 9999999;
  float M_jet2_muon2 = 9999999;
  float mT_jet2_muon2 = 9999999;
  float dR_jet2_muon2 = 9999999;
  float M_jet2_muon3 = 9999999;
  float mT_jet2_muon3 = 9999999;
  float dR_jet2_muon3 = 9999999;
  float M_jet2_ele1 = 9999999;
  float mT_jet2_ele1 = 9999999;
  float dR_jet2_ele1 = 9999999;
  float M_jet2_ele2 = 9999999;
  float mT_jet2_ele2 = 9999999;
  float dR_jet2_ele2 = 9999999;
  if(njet>=2 && nmuon>=1){
    auto H_TLV = jet2_TLV+muon1_TLV;
    M_jet2_muon1 = H_TLV.M();
    
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet2_muon1  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));

    double dEta_jet2_muon1 = fabs(muon1_eta-jet2_eta);
    double dPhi_jet2_muon1 = fabs(muon1_phi-jet2_phi);
    if (dPhi_jet2_muon1 >= TMath::Pi()) dPhi_jet2_muon1 = 2*TMath::Pi()-dPhi_jet2_muon1;
    dR_jet2_muon1 = sqrt(dEta_jet2_muon1*dEta_jet2_muon1+dPhi_jet2_muon1*dPhi_jet2_muon1);
  }

  if(njet>=2 && nmuon>=2){
    auto H_TLV = jet2_TLV+muon2_TLV;
    M_jet2_muon2 = H_TLV.M();
    
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet2_muon2  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));

    double dEta_jet2_muon2 = fabs(muon2_eta-jet2_eta);
    double dPhi_jet2_muon2 = fabs(muon2_phi-jet2_phi);
    if (dPhi_jet2_muon2 >= TMath::Pi()) dPhi_jet2_muon2 = 2*TMath::Pi()-dPhi_jet2_muon2;
    dR_jet2_muon2 = sqrt(dEta_jet2_muon2*dEta_jet2_muon2+dPhi_jet2_muon2*dPhi_jet2_muon2);
  }

  if(njet>=2 && nmuon>=3){
    auto H_TLV = jet2_TLV+muon3_TLV;
    M_jet2_muon3 = H_TLV.M();
    
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet2_muon3  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));

    double dEta_jet2_muon3 = fabs(muon3_eta-jet2_eta);
    double dPhi_jet2_muon3 = fabs(muon3_phi-jet2_phi);
    if (dPhi_jet2_muon3 >= TMath::Pi()) dPhi_jet2_muon3 = 2*TMath::Pi()-dPhi_jet2_muon3;
    dR_jet2_muon3 = sqrt(dEta_jet2_muon3*dEta_jet2_muon3+dPhi_jet2_muon3*dPhi_jet2_muon3);
  }

  if(njet>=2 && nele>=1){
    auto H_TLV = jet2_TLV+ele1_TLV;
    M_jet2_ele1 = H_TLV.M();
    
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet2_ele1  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));

    double dEta_jet2_ele1 = fabs(ele1_eta-jet2_eta);
    double dPhi_jet2_ele1 = fabs(ele1_phi-jet2_phi);
    if (dPhi_jet2_ele1 >= TMath::Pi()) dPhi_jet2_ele1 = 2*TMath::Pi()-dPhi_jet2_ele1;
    dR_jet2_ele1 = sqrt(dEta_jet2_ele1*dEta_jet2_ele1+dPhi_jet2_ele1*dPhi_jet2_ele1);
  }

  if(njet>=2 && nele>=2){
    auto H_TLV = jet2_TLV+ele2_TLV;
    M_jet2_ele2 = H_TLV.M();
    
    auto ET=sqrt(H_TLV.M()* H_TLV.M()+ H_TLV.Pt()* H_TLV.Pt());
    double deltaPhi = fabs(H_TLV.Phi()-METCand->phi());
    if (deltaPhi >= TMath::Pi()) deltaPhi = 2*TMath::Pi()-deltaPhi;
    mT_jet2_ele2  = sqrt(2*ET*METCand->met()*(1-cos(deltaPhi)));

    double dEta_jet2_ele2 = fabs(ele2_eta-jet2_eta);
    double dPhi_jet2_ele2 = fabs(ele2_phi-jet2_phi);
    if (dPhi_jet2_ele2 >= TMath::Pi()) dPhi_jet2_ele2 = 2*TMath::Pi()-dPhi_jet2_ele2;
    dR_jet2_ele2 = sqrt(dEta_jet2_ele2*dEta_jet2_ele2+dPhi_jet2_ele2*dPhi_jet2_ele2);
  }
  DEBUGclass("jet2 dR,M,mT done");

  //M_ep1em1
  float M_ep1em1 = 9999999;
  if(nele_p>=1 && nele_m>=1){
    auto ep1em1_TLV = ele1_p_TLV + ele1_m_TLV;
    M_ep1em1 = ep1em1_TLV.M();
    DEBUGclass("M_ep1em1:%f",M_ep1em1);
  }
  DEBUGclass("M_ep1em1 done");
  
  //M_up1um1
  float M_up1um1 = 9999999;
  if(nmuon_p>=1 && nmuon_m>=1){
    auto up1um1_TLV = muon1_p_TLV + muon1_m_TLV;
    M_up1um1 = up1um1_TLV.M();
    DEBUGclass("M_up1um1:%f",M_up1um1);
  }
  DEBUGclass("M_up1um1 done");

  //M_l1l2
  float M_l1l2 = 9999999;
  if(nlep>=2){
    auto l1l2_TLV = lep1_TLV + lep2_TLV;
    M_l1l2 = l1l2_TLV.M();
  }
  DEBUGclass("M_l1l2 done");

  //W_mT
  float W_mT_muon1 =9999999;
  float W_mT_muon2 =9999999;
  float W_mT_muon3 =9999999;
  float W_mT_ele1 =9999999;
  float W_mT_ele2 =9999999;
  float W_mT_noZcUpair_Hlep =9999999;
  float W_mT_noZcEpair_Hlep =9999999;
  if (veclep_no_Zc_uPair.size()>=1){
    auto ET_lep=sqrt(noZcUpair_Hlep_TLV.M()*noZcUpair_Hlep_TLV.M()+noZcUpair_Hlep_TLV.Pt()*noZcUpair_Hlep_TLV.Pt());
    auto dPhi_l_miss = fabs(noZcUpair_Hlep_TLV.Phi()-METCand->phi());
    if (dPhi_l_miss>= TMath::Pi()) dPhi_l_miss = 2*TMath::Pi()-dPhi_l_miss;
    W_mT_noZcUpair_Hlep = sqrt(2*ET_lep*METCand->met()*(1-cos(dPhi_l_miss)));
  }

  if (veclep_no_Zc_ePair.size()>=1){
    auto ET_lep=sqrt(noZcEpair_Hlep_TLV.M()*noZcEpair_Hlep_TLV.M()+noZcEpair_Hlep_TLV.Pt()*noZcEpair_Hlep_TLV.Pt());
    auto dPhi_l_miss = fabs(noZcEpair_Hlep_TLV.Phi()-METCand->phi());
    if (dPhi_l_miss>= TMath::Pi()) dPhi_l_miss = 2*TMath::Pi()-dPhi_l_miss;
    W_mT_noZcEpair_Hlep = sqrt(2*ET_lep*METCand->met()*(1-cos(dPhi_l_miss)));
  }
  
  if(nmuon>=1){
    auto ET_lep=sqrt(muon1_TLV.M()*muon1_TLV.M()+muon1_TLV.Pt()*muon1_TLV.Pt());
    auto dPhi_l_miss = fabs(muon1_TLV.Phi()-METCand->phi());
    if (dPhi_l_miss>= TMath::Pi()) dPhi_l_miss = 2*TMath::Pi()-dPhi_l_miss;
    W_mT_muon1 = sqrt(2*ET_lep*METCand->met()*(1-cos(dPhi_l_miss)));
  }

  if(nmuon>=2){
    auto ET_lep=sqrt(muon2_TLV.M()*muon2_TLV.M()+muon2_TLV.Pt()*muon2_TLV.Pt());
    auto dPhi_l_miss = fabs(muon2_TLV.Phi()-METCand->phi());
    if (dPhi_l_miss>= TMath::Pi()) dPhi_l_miss = 2*TMath::Pi()-dPhi_l_miss;
    W_mT_muon2 = sqrt(2*ET_lep*METCand->met()*(1-cos(dPhi_l_miss)));
  }

  if(nmuon>=3){
    auto ET_lep=sqrt(muon3_TLV.M()*muon3_TLV.M()+muon3_TLV.Pt()*muon3_TLV.Pt());
    auto dPhi_l_miss = fabs(muon3_TLV.Phi()-METCand->phi());
    if (dPhi_l_miss>= TMath::Pi()) dPhi_l_miss = 2*TMath::Pi()-dPhi_l_miss;
    W_mT_muon3 = sqrt(2*ET_lep*METCand->met()*(1-cos(dPhi_l_miss)));
  }

  if(nele>=1){
    auto ET_lep=sqrt(ele1_TLV.M()*ele1_TLV.M()+ele1_TLV.Pt()*ele1_TLV.Pt());
    auto dPhi_l_miss = fabs(ele1_TLV.Phi()-METCand->phi());
    if (dPhi_l_miss>= TMath::Pi()) dPhi_l_miss = 2*TMath::Pi()-dPhi_l_miss;
    W_mT_ele1 = sqrt(2*ET_lep*METCand->met()*(1-cos(dPhi_l_miss)));
  }
  

  if(nele>=2){
    auto ET_lep=sqrt(ele2_TLV.M()*ele2_TLV.M()+ele2_TLV.Pt()*ele2_TLV.Pt());
    auto dPhi_l_miss = fabs(ele2_TLV.Phi()-METCand->phi());
    if (dPhi_l_miss>= TMath::Pi()) dPhi_l_miss = 2*TMath::Pi()-dPhi_l_miss;
    W_mT_ele2 = sqrt(2*ET_lep*METCand->met()*(1-cos(dPhi_l_miss)));
  }
  DEBUGclass("W_mT done");
  //dPt_jet1_jet2,dEta_jet1_jet2,dPhi_jet1_jet2
  float dPt_jet1_jet2 =9999999;
  float dEta_jet1_jet2 =9999999;
  float dPhi_jet1_jet2 =9999999;
  if(njet>=2){
    dPt_jet1_jet2=fabs(jet1_pt-jet2_pt);
    dEta_jet1_jet2=fabs(jet1_eta-jet2_eta);
    dPhi_jet1_jet2=fabs(jet1_phi-jet2_phi);
    if (dPhi_jet1_jet2>= TMath::Pi()) dPhi_jet1_jet2 = 2*TMath::Pi()-dPhi_jet1_jet2;
  }
  DEBUGclass("d_jet1_jet2 done");
  DEBUGclass("============================ event info end =====================");
  //number
  myvector.push_back(njet);
  myvector.push_back(nmuon);
  myvector.push_back(nele);
  myvector.push_back(nmuon_p);
  myvector.push_back(nmuon_m);
  myvector.push_back(nele_p);
  myvector.push_back(nele_m);
  myvector.push_back(nlep);//7

  //pt
  myvector.push_back(jet1_pt);
  myvector.push_back(jet2_pt);
  
  myvector.push_back(muon1_pt);
  myvector.push_back(muon2_pt);
  myvector.push_back(muon3_pt);//12
  
  myvector.push_back(ele1_pt);
  myvector.push_back(ele2_pt);
  
  myvector.push_back(lep1_pt);
  myvector.push_back(lep2_pt);
  myvector.push_back(lep3_pt);//17

  //eta
  myvector.push_back(jet1_eta);
  myvector.push_back(jet2_eta);
  
  myvector.push_back(muon1_eta);
  myvector.push_back(muon2_eta);
  myvector.push_back(muon3_eta);//22
  
  myvector.push_back(ele1_eta);
  myvector.push_back(ele2_eta);
  
  myvector.push_back(lep1_eta);
  myvector.push_back(lep2_eta);
  myvector.push_back(lep3_eta);//27

  //phi
  myvector.push_back(jet1_phi);
  myvector.push_back(jet2_phi);
  
  myvector.push_back(muon1_phi);
  myvector.push_back(muon2_phi);
  myvector.push_back(muon3_phi);//32
  
  myvector.push_back(ele1_phi);
  myvector.push_back(ele2_phi);
  
  myvector.push_back(lep1_phi);
  myvector.push_back(lep2_phi);
  myvector.push_back(lep3_phi);//37

  //mass
  myvector.push_back(jet1_mass);
  myvector.push_back(jet2_mass);//39

  //flavor
  myvector.push_back(lep1_flavor);
  myvector.push_back(lep2_flavor);
  myvector.push_back(lep3_flavor);//42

  //charge
  myvector.push_back(lep1_charge);
  myvector.push_back(lep2_charge);
  myvector.push_back(lep3_charge);//45

  myvector.push_back(muon1_charge);
  myvector.push_back(muon2_charge);
  myvector.push_back(muon3_charge);//48

  myvector.push_back(ele1_charge);
  myvector.push_back(ele2_charge);//50

  //dR
  myvector.push_back(dR_jet1_muon1);
  myvector.push_back(dR_jet1_muon2);
  myvector.push_back(dR_jet1_muon3);

  myvector.push_back(dR_jet1_ele1);
  myvector.push_back(dR_jet1_ele2);//55

  myvector.push_back(dR_jet2_muon1);
  myvector.push_back(dR_jet2_muon2);
  myvector.push_back(dR_jet2_muon3);

  myvector.push_back(dR_jet2_ele1);
  myvector.push_back(dR_jet2_ele2);//60
  
  //composite M jet-lep
  myvector.push_back(M_jet1_muon1);
  myvector.push_back(M_jet1_muon2);
  myvector.push_back(M_jet1_muon3);

  myvector.push_back(M_jet1_ele1);
  myvector.push_back(M_jet1_ele2);//65

  myvector.push_back(M_jet2_muon1);
  myvector.push_back(M_jet2_muon2);
  myvector.push_back(M_jet2_muon3);

  myvector.push_back(M_jet2_ele1);
  myvector.push_back(M_jet2_ele2);//70

  //composite mT jet-lep
  myvector.push_back(mT_jet1_muon1);
  myvector.push_back(mT_jet1_muon2);
  myvector.push_back(mT_jet1_muon3);

  myvector.push_back(mT_jet1_ele1);
  myvector.push_back(mT_jet1_ele2);//75

  myvector.push_back(mT_jet2_muon1);
  myvector.push_back(mT_jet2_muon2);
  myvector.push_back(mT_jet2_muon3);

  myvector.push_back(mT_jet2_ele1);
  myvector.push_back(mT_jet2_ele2);//80

  //composite M lep-lep
  myvector.push_back(M_ep1em1);
  myvector.push_back(M_up1um1);
  myvector.push_back(M_l1l2);//83

  //mT W
  myvector.push_back(W_mT_muon1);
  myvector.push_back(W_mT_muon2);
  myvector.push_back(W_mT_muon3);

  myvector.push_back(W_mT_ele1);
  myvector.push_back(W_mT_ele2);//88

  //j1j2
  myvector.push_back(dPt_jet1_jet2);
  myvector.push_back(dEta_jet1_jet2);
  myvector.push_back(dPhi_jet1_jet2);//91

  //jet1_noZcUpair_Hlep
  myvector.push_back(M_Zc_uPair);
  myvector.push_back(M_jet1_noZcUpair_Hlep);
  myvector.push_back(dR_jet1_noZcUpair_Hlep);
  myvector.push_back(W_mT_noZcUpair_Hlep);
  myvector.push_back(mT_jet1_noZcUpair_Hlep);//96

  //jet1_noZcEpair_Hlep
  myvector.push_back(M_Zc_ePair);
  myvector.push_back(M_jet1_noZcEpair_Hlep);
  myvector.push_back(dR_jet1_noZcEpair_Hlep);
  myvector.push_back(W_mT_noZcEpair_Hlep);
  myvector.push_back(mT_jet1_noZcEpair_Hlep);//101

  // bookmark cached entry
  this->fCachedEntry = this->getCurrentEntry();
  DEBUGclass("Created new set of cached values in instance '%s'",this->GetName());
  return true;
#endif
}

//______________________________________________________________________________________________

const std::vector<double>* ZHCand::getVector() const {
  // Function to retrieve the cached vector
  /* example block
     return &myvector;
  */
  
     return &myvector;
  //this should never be executed, we just make the compiler calm down
  throw std::runtime_error("Illegal / unsupported mode selected!");
  return NULL;
}

//______________________________________________________________________________________________

double ZHCand::getValue() const {
  // Value retrieval function, called on every event for every cut and histogram
  // Since this is a vector observable this should never be executed
  DEBUGclass("entering function getValue");
  throw std::runtime_error("Caught attempt to evaluate vector valued observable in scalar context");
  return std::numeric_limits<double>::quiet_NaN();
}

//______________________________________________________________________________________________

double ZHCand::getValueAt(int index) const {
  // Function to return element of vector with specified index
  if (!this->makeCache()) {
    ERROR("Failed to obtain return values!");
    return std::numeric_limits<double>::quiet_NaN();
  }

  const std::vector<double>* vec = this->getVector();
  if ((int)vec->size() == 0) { // vector is empty, not even filled once
    throw std::runtime_error("Attempted to retrieve value of vector that is empty");
    return std::numeric_limits<double>::quiet_NaN(); 
  }
  if (index >= (int)vec->size()) {
    throw std::runtime_error("Attempted to retrieve value for out of bounds");
    return std::numeric_limits<double>::quiet_NaN();
  }
  return vec->at(index);
}

//______________________________________________________________________________________________

int ZHCand::getNevaluations() const {
  // Function to return the size of the vector that is going to be retrieved
  
  if (!this->makeCache()) {
    ERROR("Failed to obtain return values!");
    return -1;
  }
  if (this->fType==TQObservable::ObservableType::scalar) return 1;
  const std::vector<double>* vec = this->getVector();
  if (vec) return vec->size();
  return -1;
}
//______________________________________________________________________________________________

bool ZHCand::initializeSelf(){
  // initialize this observable
  // called once per sample (input file) so that the observable knows the name of the event candidate
  // will be EventEM or EventME in the case of DF analysis (depending on the current channel)
  // example block
     TString ContName = "";
     if(!this->fSample->getTagString("~cand",ContName)) return false;
     this->mContName = "Event"+ContName;
  

  DEBUGclass("initializing");
  
  fCachedEntry = -1;
  return true;
}

//______________________________________________________________________________________________

bool ZHCand::finalizeSelf(){
  // finalize this observable
  // remember to undo anything you did in initializeSelf() !
  /* example block
     myvector.clear();
  */
  
  myvector.clear();
  DEBUGclass("finalizing");
  return true;
}
//______________________________________________________________________________________________

ZHCand::ZHCand(const TString& name):
TQEventObservable(name)
{
  // constructor with name argument
  DEBUGclass("constructor called with '%s'",name.Data());
}

bool ZHCand :: cmp1 (std::tuple<std::string, int, float> a, std::tuple<std::string, int, float> b)
{
    return std::get<2>(a) > std::get<2>(b);
}
bool ZHCand :: cmp2 (std::tuple<std::string, std::string, int, float> a, std::tuple<std::string,std::string, int, float> b)
{
    return std::get<3>(a) > std::get<3>(b);
}

